import { Component } from 'inferno';

import * as XLSX from 'xlsx';
import * as FileSaver from 'file-saver';

import TableComponent from './TableComponent';

import { dummy } from '../dummy-data/grocery-list';


const _BOOK_TYPE = 'xlsx';
const _TYPE = 'array';
const _XLSX_MIME_TYPE = 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet';
const _EXCEL_TYPE = `${_XLSX_MIME_TYPE};charset=UTF-8`;

export default class TableExportComponent extends Component {

  exportTableToFile = () => {
    const worksheet = XLSX.utils.json_to_sheet(dummy);

    const workbook = { Sheets: { data: worksheet }, SheetNames: ['data'] };

    const buffer = XLSX.write(workbook, { bookType: _BOOK_TYPE, type: _TYPE });

    const data = new Blob([buffer], { type: _EXCEL_TYPE });

    FileSaver.saveAs(data, `test-excel-export`);
  }

  render() {
    return (
      <div>
          <button onClick={this.exportTableToFile}>
              Export XLXS
          </button>

          <TableComponent json={dummy}></TableComponent>
      </div>
    );
  }
}
