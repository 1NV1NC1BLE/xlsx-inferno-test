import { Component } from 'inferno';
import './table.css';

export default class TableComponent extends Component {

    createHeader = (headerList = []) => {
        console.log(headerList)
        return headerList.map((header, index) => {
            return (
                <th className="th">{header}</th>
            )
        })
    }

    createRow = (data = []) => {
        return data.map((rowData) => {
            return (
                <tr>
                    {Object.values(rowData).map(dtl => (<td className="td">{dtl}</td>))}
                </tr>
            )
        })
    }

    render() {
        const { json } = this.props;

        if (json === 'undefined' || json === null) {
            return (<div></div>);
        }

        return (
            <table className="table">

                <thead className="th">
                    <tr>
                    {json[0] ? this.createHeader(Object.keys(json[0])) : (<td></td>) }
                    </tr>
                </thead>
                <tbody>
                    {this.createRow(json)}
                </tbody>
            </table>
        );
    }
}

