import { Component } from 'inferno';

import * as XLSX from 'xlsx';

import TableComponent from './TableComponent';

export default class ImportComponent extends Component {

    state = {
        excelData: [],
    }

    handleFile = (e) => {
        var files = e.target.files, f = files[0];
        var reader = new FileReader();
        reader.onload = (e) => {
            var data = new Uint8Array(e.target.result);
            var wb = XLSX.read(data, { type: 'array' });


            const wsname = wb.SheetNames[0];
            const ws = wb.Sheets[wsname];

            const wsData = XLSX.utils.sheet_to_json(ws);

            
            console.log("header: 1", wsData)
            console.log("no-header", XLSX.utils.sheet_to_json(ws))
            console.log("header: 'A", XLSX.utils.sheet_to_json(ws, {header:"A"}))
            console.log("specific header", XLSX.utils.sheet_to_json(ws, { header: ["A","E","I","O","U","6","9"] }))

            this.setState({ excelData: wsData });

        };
        if(f){
            reader.readAsArrayBuffer(f);
        }
        
    }

    render() {

        const { excelData } = this.state;
        return (
            <div>
                <input type="file" onChange={this.handleFile}></input>
                
                <div style={{height: '100px'}}></div>

                <TableComponent json={excelData}></TableComponent>

            </div>
        );
    }
}
