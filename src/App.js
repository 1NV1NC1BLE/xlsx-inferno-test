import { Component } from 'inferno';
import './App.css';

import ExportComponent from './xlxs/ExportComponent';
import ImportComponent from './xlxs/ImportComponent';

class App extends Component {

  render() {
    return (
      <div className="App">
        <div style={center}>
          <h3>Export Demo</h3>
          <ExportComponent></ExportComponent>
        </div>

        <div style={divider}></div>

        <div style={center}>
          <h3>Import Demo</h3>
          <ImportComponent></ImportComponent>
        </div>

      </div>
    );
  }
}

const center = {
  textAlign: 'center',
}

const divider = {
  height: '100px'
}

export default App;
