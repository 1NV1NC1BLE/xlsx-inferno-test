export const settings = [
    {
        key: 'id',
        label: 'Item ID',
    },
    {
        key: 'name',
        label: 'Item Name',
    },
    {
        key: 'type',
        label: 'Type',
    },
    {
        key: 'quantity',
        label: 'Quantity',
    },
    {
        currencyKey: 'currency',
        decimal: 2,
        key: 'price',
        label: 'Price',
    },
    {
        key: 'crtOn',
        label: 'Created On',
    },
    {
        key: 'crtBy',
        label: 'Created By',
    },
    {
        fn: val => {
            return (val.status === 'B' ? 'Bought' : 'Pending');
        },
        key: 'status',
        label: 'Status',
    },

];